      .data 0x10000000
char1: .byte 'a' # reserve space for a byte
double1: .double 1.1 # reserve space for a double
char2: .byte 'b' # b is 0x62 in ASCII
half1: .half 0x8001 # reserve space for a half-word (2 bytes)
char3: .byte 'c' # c is 0x63 in ASCII
word1: .word 0x56789abc # reserve space for a word
char4: .byte 'd' # d is 0x64 in ASCII
word2: .word 0 # reserve space for a word
.text
.globl main
main:
la $4, word1
lb $t3, 0($4)
lb $t2, 1($4)
lb $t1, 2($4)
lb $t0, 3($4)
lbu $t7, 0($4)
lbu $t6, 1($4)
lbu $t5, 2($4)
lbu $t4, 3($4)
lh $t8, half1
lhu $t9, half1
la $4, word2
sb $t4, 0($4)
sb $t5, 1($4)
sb $t6, 2($4)
sb $t7, 3($4)
lw $t9, word2

jr $ra # return from main

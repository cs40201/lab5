.data
	.align 0

ch1: 	.byte 'a'
word1: 	.word 0x89abcdef
ch2: 	.byte 'b'
word2:	.word 0
	
	.text
	.globl main

main:
	la $a0, word1	
	la $a1, word2   
	
	lwr $t1, 0($a0) 
	lwl $t1, 3($a0)

	swr $t1, 0($a1)	
	swl $t1, 3($a1) 

	jr $ra

.data 

word1: .word 0x89abcdef

.text
.globl main

main:	la $a0, word1

	lwl $t0, 3($a0)
	lwl $t1, 2($a0)
	lwl $t2, 1($a0)
	lwl $t3, 0($a0)
		
	jr $ra

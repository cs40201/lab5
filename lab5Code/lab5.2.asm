      .data 0x10000000
double1: .double 1.1 # reserve space for a double
char1: .byte 'a' # reserve space for a byte
char2: .byte 'b' # b is 0x62 in ASCII
char3: .byte 'c' # c is 0x63 in ASCII
char4: .byte 'd' # d is 0x64 in ASCII
word1: .word 0x56789abc # reserve space for a word
half1: .half 0x8001 # reserve space for a half-word (2 bytes)
.text
.globl main
main:
l.d $f2, double1
lb $t0, char1
lb $t1, char2
lb $t3, char3
lb $t5, char4 
lw $t4, word1
lh $t2, half1

jr $ra # return from main

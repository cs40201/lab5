      .data 0x10000000
user1: .word 0
msg1: .asciiz "Please enter an integer number: "
msg3: .asciiz "If bytes were layed in reverse order the number would be: "
.text
.globl main
main: add $sp, $sp, -4 # must save $ra since I’ll have a call
      sw $ra, 4($sp)      
      li $v0, 4 # system call for print_str
      la $a0, msg1 # address of string to print
      syscall # now get an integer from the user
      li $v0, 5 # system call for read_int
      syscall # the integer placed in $v0
      # do some computation here with the number
      addu $t0, $v0, $0 # move the number in $t0
      sw $t0, user1
      la $t1, user1
      move $a0, $t1
      jal Reverse # call ‘test’ with no parameters
      nop # execute this after ‘test’ returns
      lw $ra, 4($sp) # restore the return address in $ra
      add $sp, $sp, 4
      jr $ra # return from main
      # The procedure ‘test’ does not call any other procedure. Therefore $ra
      # does not need to be saved. Since ‘test’ uses no registers there is
      # no need to save any registers.
Reverse: add $sp, $sp, -4
         sw $ra, 4($sp)
         move $t1, $a0
         lbu $t4, 0($t1)
         lbu $t5, 1($t1)
         lbu $t6, 2($t1)
         lbu $t7, 3($t1)
         sb $t7, 0($t1)
         sb $t6, 1($t1)
         sb $t5, 2($t1)
         sb $t4, 3($t1)
         lw $t0, user1
         li $v0, 4 # system call for print_str
         la $a0, msg3 # address of string to print
         syscall
         li $v0, 1 # system call for print_int
         addu $a0, $t0, $0 # move number to print in $a0
         syscall
         lw $ra, 4($sp)
         add $sp, $sp, 4
         jr $ra # return from this procedure

